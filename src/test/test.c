#include "support.h"

static volatile unsigned int counter = 0x100000;

int benchmark (void)
{
  counter = 0x1000;
  while (counter != 0)
	  counter-- ;

  return 0;
}
